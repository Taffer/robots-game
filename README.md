# Robots Game

Godot Weekly Gamedev Challenge Week 11 -
https://mastodon.gamedev.place/@gamedevchallengewithgodot/109496361250432201

This is like [Daleks](https://www.mobygames.com/game/atari-st/daleks) on the
Atari ST and a zillion other games. I'd actually thought of cloning one of
these for practice, so when it game up in the weekly challenge I figured,
"Why not?"

The rules:

![Challenge Week 11 rules](godot-gamedev-challenge-week-11.png)

The result:

[![Robots chasing humans](robots.png)](robots-big.png)

To play:

* Click to move in the direction of the arrow cursor.
* Click on the human to stay in one spot for a turn.
* Right-click to teleport to a random spot. Dangerous!

To lose:

* Get crushed by a robot.
* Stumble into a crater.

To win:

* Trick all the robots into crashing into each other or craters.

## Credits

This uses the following resources, which have their own license:

* `Kenney/arrow*.png`, `Kenney/down*.png`, `Kenney/target.png`, and
  `Kenney/up*.png` - from
  [Kenney's Game Icons](https://kenney.nl/assets/game-icons); this is covered
  by a [CC0 1.0 - Universal](https://creativecommons.org/publicdomain/zero/1.0/)
  license and was created by [Kenney](https://kenney.nl/).
* `Kenney/mapPack_tilesheet.png` - From
  [Kenney's Map Pack](https://kenney.nl/assets/map-pack); this is covered by a
  [CC0 1.0 - Universal](https://creativecommons.org/publicdomain/zero/1.0/)
  license and was created by [Kenney](https://kenney.nl/).
* `Kenney/spritesheet_pixelExplosion.png` - from
  [Kenney's Explosion Pack](https://kenney.nl/assets?q=2d); this is covered by a
  [CC0 1.0 - Universal](https://creativecommons.org/publicdomain/zero/1.0/)
  license and was created by [Kenney](https://kenney.nl/).
* `run.png` - From [game-icons.net](https://game-icons.net/1x1/lorc/run.html);
  this is covered by a [CC BY 3.0](http://creativecommons.org/licenses/by/3.0/)
  license and was created by [Lorc](https://lorcblog.blogspot.com/).

## License

MIT licensed, of course. [Details here.](LICENSE)

## To-Do

Things that will make it an actual game:

* Win condition
* Win screen
* Lose screen

Things that are "just" polish:

* How to play screen
* Animated Robots
* Animated Human
* Tween Human and Robot movement
* Better death-crater after an explosion
* Sound effects
* Music
