extends ColorRect


const NUM_ENEMIES: int = 100
const GRID_WIDTH: int = 45
const GRID_HEIGHT: int = 30

@onready var map: TileMap = $Map
var map_min: Vector2i
var map_max: Vector2i
@onready var victim: AnimatedSprite2D = $Victim
var victim_pos: Vector2i  # Human location in tilemap co-ords.
@onready var robots_label: Label = $RobotsLabel

@onready var robot: Resource = load("res://scenes/robot.tscn")
@onready var crater: Resource = load("res://scenes/boom.tscn")

var craters: Dictionary = {}
var robots: Dictionary = {}

# Mouse cursors
var cursor_left: Resource = load("res://Kenney/arrowLeft.png")
var cursor_right: Resource = load("res://Kenney/arrowRight.png")
var cursor_up: Resource = load("res://Kenney/arrowUp.png")
var cursor_down: Resource = load("res://Kenney/arrowDown.png")
var cursor_ul: Resource = load("res://Kenney/upLeft.png")
var cursor_ur: Resource = load("res://Kenney/upRight.png")
var cursor_dl: Resource = load("res://Kenney/downLeft.png")
var cursor_dr: Resource = load("res://Kenney/downRight.png")
var cursor_stay: Resource = load("res://Kenney/target.png")
@onready var cursor_original: Input.CursorShape = Input.get_current_cursor_shape()
var cursor_hotspot: Vector2 = Vector2(16.0, 16.0)

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	map_min = map.map_to_local(Vector2i(0, 0))
	map_max = map.map_to_local(map.get_used_rect().size - Vector2i(1, 1))

	# Victim starts in the centre square.
	var center: Vector2i = get_viewport_rect().get_center()
	var center_map: Vector2i = map.local_to_map(center)
	victim_pos = center_map
	victim.position = map.map_to_local(center_map)

	# Add 100 enemies in random spots; victim's location is forbidden.
	var rng: RandomNumberGenerator = RandomNumberGenerator.new()
	var count: int = 0
	while count < NUM_ENEMIES:
		var pos: Vector2i = map.local_to_map(Vector2i(rng.randi_range(0, map_max.x), rng.randi_range(0, map_max.y)))
		if pos != center_map:
			if robots.has(pos):
				print("Robot go boom ", count)
				remove_child(robots[pos])
				robots.erase(pos)
				make_boom(pos)
			elif craters.has(pos):
				print("Robot in crater, boom ", count)
				make_boom(pos)
			else:
				# Clear spot, create a new robot here.
				var enemy: AnimatedSprite2D = robot.instantiate()
				robots[pos] = enemy
				enemy.position = map.map_to_local(pos)
				add_child(enemy)
			count += 1

	update_robot_count()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta: float) -> void:
	if Input.is_action_just_pressed("ui_cancel"):
		get_tree().quit()

	if Input.is_action_just_pressed("ui_accept"):
		move_by_teleport()
		return

	# TODO: Diagonal movement.
	var move_up: bool = Input.is_action_just_pressed("ui_up")
	var move_down: bool = Input.is_action_just_pressed("ui_down")
	var move_left: bool = Input.is_action_just_pressed("ui_left")
	var move_right: bool = Input.is_action_just_pressed("ui_right")

	if move_up and move_down:
		move_up = false
		move_down = false
	if move_left and move_right:
		move_left = false
		move_right = false

	if (move_up or move_down or move_left or move_right):
		var current: Vector2i = map.local_to_map(victim.position)
		if move_up:
			current.y -= 1
		if move_down:
			current.y += 1
		if move_left:
			current.x -= 1
		if move_right:
			current.x += 1

		move_player_to(current)


func _input(event: InputEvent) -> void:
	if event is InputEventMouseButton and event.pressed:
		if event.button_index == 1:
			# Move the player via the main button.
			move_by_click(event.position)
		elif event.button_index != 0:
			# Any other button is teleport.
			move_by_teleport()
	elif event is InputEventMouseMotion:
		# Where is mouse?
		set_mouse_cursor(event.position)


func move_by_click(position: Vector2) -> void:
	var pos_map: Vector2i = map.local_to_map(position)
	var current: Vector2i = map.local_to_map(victim.position)

	if pos_map.y < victim_pos.y:
		# Mouse above victim.
		if pos_map.x == victim_pos.x:
			current.y -= 1
		elif pos_map.x < victim_pos.x:
			current.x -= 1
			current.y -= 1
		else:
			current.x += 1
			current.y -= 1
	elif pos_map.y > victim_pos.y:
		# Mouse below victim.
		if pos_map.x == victim_pos.x:
			current.y += 1
		elif pos_map.x < victim_pos.x:
			current.x -= 1
			current.y += 1
		else:
			current.x += 1
			current.y += 1
	else:  # pos_map.y == victim_pos.y
		if pos_map.x == victim_pos.x:
			# Stay put.
			pass
		elif pos_map.x < victim_pos.x:
			current.x -= 1
		else:
			current.x += 1

	move_player_to(current)


func  move_by_teleport() -> void:
	var rng: RandomNumberGenerator = RandomNumberGenerator.new()
	var pos: Vector2i = map.local_to_map(Vector2i(rng.randi_range(0, map_max.x), rng.randi_range(0, map_max.y)))

	# TODO: Teleport FX
	print('PLAYER TELEPORTED: %s' % pos)

	move_player_to(pos)


func set_mouse_cursor(position: Vector2) -> void:
	var pos_map: Vector2i = map.local_to_map(position)

	if pos_map.y < victim_pos.y:
		# Mouse above victim.
		if pos_map.x == victim_pos.x:
			Input.set_custom_mouse_cursor(cursor_up, Input.CURSOR_ARROW, cursor_hotspot)
		elif pos_map.x < victim_pos.x:
			Input.set_custom_mouse_cursor(cursor_ul, Input.CURSOR_ARROW, cursor_hotspot)
		else:
			Input.set_custom_mouse_cursor(cursor_ur, Input.CURSOR_ARROW, cursor_hotspot)
	elif pos_map.y > victim_pos.y:
		# Mouse below victim.
		if pos_map.x == victim_pos.x:
			Input.set_custom_mouse_cursor(cursor_down, Input.CURSOR_ARROW, cursor_hotspot)
		elif pos_map.x < victim_pos.x:
			Input.set_custom_mouse_cursor(cursor_dl, Input.CURSOR_ARROW, cursor_hotspot)
		else:
			Input.set_custom_mouse_cursor(cursor_dr, Input.CURSOR_ARROW, cursor_hotspot)
	else:  # pos_map.y == victim_pos.y
		if pos_map.x == victim_pos.x:
			Input.set_custom_mouse_cursor(cursor_stay, Input.CURSOR_ARROW, cursor_hotspot)
		elif pos_map.x < victim_pos.x:
			Input.set_custom_mouse_cursor(cursor_left, Input.CURSOR_ARROW, cursor_hotspot)
		else:
			Input.set_custom_mouse_cursor(cursor_right, Input.CURSOR_ARROW, cursor_hotspot)


func move_player_to(current: Vector2i) -> void:
	if craters.has(current):
		print("VICTIM IN CRATER")

		print("YOU DIED")  # TODO: Real Game Over screen.
		make_boom(current)
		get_tree().quit()

	elif robots.has(current):
		print("VICTIM vs ROBOT")
		remove_child(robots[current])
		robots.erase(current)

		make_boom(current)

		print("YOU DIED")  # TODO: Real Game Over screen.
		get_tree().quit()

	victim.position = map.map_to_local(current).clamp(map_min, map_max)
	victim_pos = current

	move_robots()


func make_boom(pos: Vector2i) -> void:
	if !craters.has(pos):
		var boom: AnimatedSprite2D = crater.instantiate()
		craters[pos] = boom
		boom.position = map.map_to_local(pos)
		add_child(boom)
	else:
		# Reset existing boom.
		craters[pos].frame = 0


func update_robot_count() -> void:
	robots_label.text = 'Robots: %s' % str(len(robots))


func move_robots() -> void:
	var new_bots: Dictionary = {}

	for k in robots.keys():
		var move: Vector2i = Vector2i(0, 0)
		var enemy: AnimatedSprite2D = robots[k]
		var enemy_pos: Vector2i = map.local_to_map(enemy.position)

		# Calculate difference in x, difference in y, and diagonal; move along
		# the longest vector.
		var dx: int = victim_pos.x - enemy_pos.x
		var dy: int = victim_pos.y - enemy_pos.y
		var delta: float = sqrt(dx * dx + dy * dy)

		if delta > abs(dx) and delta > abs(dy):
			move.x = -1 if dx < 0 else 1
			move.y = -1 if dy < 0 else 1
		elif abs(dx) > abs(dy):
			move.x = -1 if dx < 0 else 1
		else:
			move.y = -1 if dy < 0 else 1

		enemy_pos += move

		if new_bots.has(enemy_pos):
			print("Robot go boom ", enemy_pos)
			make_boom(enemy_pos)
			remove_child(enemy)
			remove_child(new_bots[enemy_pos])
			new_bots.erase(enemy_pos)
		elif craters.has(enemy_pos):
			print("Robot in crater, boom ", enemy_pos)
			make_boom(enemy_pos)
			remove_child(enemy)
		elif enemy_pos == victim_pos:
			print("ROBOT ON PLAYER, DIE")
			remove_child(enemy)
			make_boom(enemy_pos)
			# TODO: player died
			get_tree().quit()
		else:
			enemy.position = map.map_to_local(enemy_pos)
			new_bots[enemy_pos] = enemy

	robots = new_bots

	update_robot_count()
